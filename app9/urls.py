from django.shortcuts import render
from django.urls import path
from . import views

app_name = 'app9'

# Create your views here.
urlpatterns = [
    path('', views.index, name="app9index"),
    path('register/', views.register, name='app9register')
]